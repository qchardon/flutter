import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/album_page.dart';
import 'package:flutter_app/model/album.dart';
import 'package:flutter_app/photos_page.dart';
import 'package:http/http.dart' as http;
import 'model/user.dart';

class AlbumPage extends StatefulWidget {
  int userId;
  String userName;

  AlbumPage({Key? key, this.userId = 0, this.userName = ""}) : super(key: key);

  @override
  _AlbumPageState createState() => _AlbumPageState();
}

class _AlbumPageState extends State<AlbumPage> {
  Widget myBuilderForAlbum(
      BuildContext context, AsyncSnapshot<dynamic> snapshot) {
    if (snapshot.hasData) {
      return ListView.builder(
        itemCount: snapshot.data.length,
        itemBuilder: (context, albumIndex) {
          final album = snapshot.data[albumIndex];
          return InkWell(
            child: Column(
              children: [
                Text("id : ${album.id}"),
                Text("nom : ${album.name}"),
              ],
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PhotosPage(
                          idAlbum: album.id,
                        )),
              );
            },
          );
        },
      );
    } else if (snapshot.hasError) {
      return Text("Erreur de chargement");
    } else {
      return CircularProgressIndicator();
    }
  }

  Future<List<Album>> _fetchAlbum() async {
    var url = Uri.parse(
        "https://jsonplaceholder.typicode.com/users/${widget.userId}/albums");
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final List albumJsonList = jsonDecode(response.body);
      return albumJsonList
          .map((albumJsonMap) => Album.fromJSON(albumJsonMap))
          .toList(); // utilisation de la fonction map pour faire de chaque case du tableau un reel objet album
    } else {
      throw Exception("Erreur de chargement des données");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Album de ${widget.userName}'),
      ),
      body: Center(
        child: FutureBuilder(
          builder: myBuilderForAlbum,
          future: _fetchAlbum(),
        ),
      ),
    );
  }
}
