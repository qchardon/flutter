class Photos {
  final int albumId;
  final int id;
  final String title;
  final String url;
  final String thumbnailUrl;

  Photos(
      {this.albumId = 0,
      this.id = 0,
      this.title = "",
      this.url = "",
      this.thumbnailUrl = ""});

  Photos.fromJSON(Map<String, dynamic> json)
      : albumId = json["albumId"],
        id = json["id"],
        title = json["title"],
        url = json["url"],
        thumbnailUrl = json["thumbnailUrl"];
}
