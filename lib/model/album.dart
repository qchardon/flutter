class Album {
  final int id;
  final String name;

  Album({this.id = 0, this.name = ""});

  Album.fromJSON(Map<String, dynamic> json)
      : id = json["id"],
        name = json["title"];
}
