import 'dart:convert';

import 'package:flutter/material.dart';

import 'model/photos.dart';
import 'package:http/http.dart' as http;

class PhotosPage extends StatefulWidget {
  int idAlbum;
  PhotosPage({Key? key, this.idAlbum = 0}) : super(key: key);

  @override
  _PhotosPageState createState() => _PhotosPageState();
}

class _PhotosPageState extends State<PhotosPage> {
  Widget myBuilderForPhotos(
      BuildContext context, AsyncSnapshot<dynamic> snapshot) {
    if (snapshot.hasData) {
      return ListView.builder(
        itemCount: snapshot.data.length,
        itemBuilder: (context, photosIndex) {
          final Photos photo = snapshot.data[photosIndex];
          return InkWell(
            child: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Row(
                children: [
                  Image.network(photo.thumbnailUrl),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(left: 15),
                      child: Column(
                        children: [
                          Text("id : ${photo.id}"),
                          Text("Title : ${photo.title}"),
                        ],
                        crossAxisAlignment: CrossAxisAlignment.start,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
    } else if (snapshot.hasError) {
      return Text("Erreur de chargement");
    } else {
      return CircularProgressIndicator();
    }
  }

  Future<List<Photos>> _fetchPhotosData() async {
    var url = Uri.parse(
        "https://jsonplaceholder.typicode.com/albums/${widget.idAlbum}/photos");
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final List photosJsonList = jsonDecode(response.body);
      return photosJsonList
          .map((photosJsonMap) => Photos.fromJSON(photosJsonMap))
          .toList();
    } else {
      throw Exception("Erreur de chargement des données");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Photos'),
      ),
      body: Center(
        child: FutureBuilder(
          builder: myBuilderForPhotos,
          future: _fetchPhotosData(),
        ),
      ),
    );
  }
}
